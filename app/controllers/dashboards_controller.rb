class DashboardsController < ApplicationController
  before_action :authenticate_user!
  layout 'dashboard'

  autocomplete :user, :name, :extra_data => [:first_name, :last_name]

  before_action :get_user

  def update
    if @user.update_attributes(user_params)
      gflash :success => "User has been updated successfully."
      redirect_to dashboard_path(current_user.id)
    else
      render :edit
    end
  end

  def search_user

  end

  private

  def get_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :picture, :phone, :address, :city, :state, :zip_code, :dob)
  end
end
