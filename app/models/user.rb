class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :picture, PictureUploader

  #METHODS
  def full_name
    "#{ first_name.try(:capitalize) } #{ last_name.try(:capitalize) }"
  end

  def full_address
    "#{address.capitalize}, #{city.capitalize}, #{state.capitalize}, #{zip_code}"
  end
end
